#    This file is part of fg-AircraftJoystick
#
#    fg-AircraftJoystick is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 2 of the License, or
#    (at your option) any later version.
#
#    fg-AircraftJoystick is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with fg-AircraftJoystick.  If not, see <http://www.gnu.org/licenses/>.
#
#	Authors: 	Dirk Dittmann
#	Date: 		01.01.2015
#
#	Last change:	Dirk Dittmann
#	Date:		02.07.2016
#

var _writeFolder 	= getprop("/sim/fg-home") ~ "/Export";
var _dlgName 		= "AircraftJoystick";

props.globals.initNode("/sim/gui/dialogs/AircraftJoystick/database/autoReload",0,"BOOL");
	

var _initMenu = func(){
	var menu 		= nil;
	var _menuIndex 		= getprop("/sim/gui/dialogs/AircraftJoystick/menu/index") or nil;
	var nDefaultMenubar	= props.globals.getNode("/sim/menubar/default/",1);
	
	foreach(var m;nDefaultMenubar.getChildren("menu")){
		if (m.getNode("label",1).getValue() == "Aircraft-Joystick"){
			_menuIndex = m.getIndex();
		}
	}
	
	
	if (_menuIndex == nil){
		menu = props.globals.getNode("/sim/menubar/default/",1).addChild("menu");
	}else{
		menu = props.globals.getNode("/sim/menubar/default/menu["~_menuIndex~"]",1);
		menu.removeAllChildren();
	}
	
	
	menu.initNode("enabled",1,"BOOL");
	menu.initNode("label","Aircraft-Joystick","STRING");
	menu.initNode("name","Menue_1","STRING");

	var item = menu.addChild("item");
	item.initNode("enabled",1,"BOOL");
	item.initNode("label","Configuration Dialog","STRING");
	item.initNode("name","Button_1","STRING");
	item.initNode("binding");
	item.initNode("binding/command","nasal","STRING");
	item.initNode("binding/script","AircraftJoystick.toggleConfigDialog();","STRING");
	
# 	item = menu.addChild("item");
# 	item.initNode("enabled",1,"BOOL");
# 	item.initNode("label","Save Config"),"STRING");
# 	item.initNode("name","Button_Save","STRING");
# 	item.initNode("binding");
# 	item.initNode("binding/command","nasal","STRING");
# 	item.initNode("binding/script","AircraftJoystick.saveConfig();","STRING");

	item = menu.addChild("item");
	item.initNode("enabled",1,"BOOL");
	item.initNode("label","Load joysticks for "~getprop("/sim/aircraft"),"STRING");
	item.initNode("name","Button_2","STRING");
	item.initNode("binding");
	item.initNode("binding/command","nasal","STRING");
	item.initNode("binding/script","AircraftJoystick.loadJoystickByAircraftName();","STRING");

	item = menu.addChild("item");
	item.initNode("enabled",1,"BOOL");
	item.initNode("label","Unload joysticks for "~getprop("/sim/aircraft"),"STRING");
	item.initNode("name","Button_unload","STRING");
	item.initNode("binding");
	item.initNode("binding/command","nasal","STRING");
	item.initNode("binding/script","AircraftJoystick.unloadAllJoysticks();","STRING");

	
# 	item = menu.addChild("item");
# 	item.initNode("enabled",1,"BOOL");
# 	item.initNode("label","nasal code reload","STRING");
# 	item.initNode("name","Button_3","STRING");
# 	item.initNode("binding");
# 	item.initNode("binding/command","nasal","STRING");
# 	item.initNode("binding/script","io.load_nasal(getprop(\"/sim/fg-home\")~\"/Nasal/AircraftJoystick.nas\");","STRING");

	setprop("/sim/gui/dialogs/AircraftJoystick/menu/index",menu.getIndex());
	fgcommand("gui-redraw");
	
};

var _showConfigDialog = func(path, toggle=0){
	var node = props.globals.getNode(path,1);
	
	var toggle = toggle > 0;
	if (toggle and contains(gui.dialog, _dlgName)) {
		fgcommand("dialog-close", props.Node.new({ "dialog-name": _dlgName }));
		delete(gui.dialog, _dlgName);
		return;
	}
	
	
	var dlg = gui.Widget.new();
	gui.dialog[_dlgName] = dlg;
	
	dlg.set("layout", "vbox");
	dlg.set("default-padding", 3);
	dlg.set("name", _dlgName);
	
	# title bar
	var titlebar = dlg.addChild("group");
	titlebar.set("layout", "hbox");
	titlebar.addChild("empty").set("stretch", 1);
	titlebar.addChild("text").set("label", "Joystick per aircraft configuration");
	titlebar.addChild("empty").set("stretch", 1);
	
	var w = titlebar.addChild("button");
	w.set("pref-width", 16);
	w.set("pref-height", 16);
	w.set("legend", "");
	w.set("default", 1);
	w.set("key", "esc");
	w.setBinding("nasal", "AircraftJoystick._onClose();");
	w.setBinding("dialog-close");
	
	dlg.addChild("hrule");
	
	var buttons = dlg.addChild("group");
	buttons.set("layout", "vbox");
	
	var c = nil;
	var b = nil;
	
	c = buttons.addChild("checkbox");
	c.set("name", "chkJoystickAutoReload");
	c.set("label", "reload joystick for aircarft on startup.");
	c.set("property", "/sim/gui/dialogs/AircraftJoystick/database/autoReload");
	c.set("live", "true");
	b = c.addChild("binding");
		b.set("command", "dialog-apply");
		b.set("object-name", "chkJoystickAutoReload");
		
		
	c = buttons.addChild("button");	
	c.set("name", "btnCopyAll");
	c.set("legend", "copy all active joysticks config from flightgear into aircraft");
	b = c.addChild("binding");
		b.set("command", "nasal");
		b.set("script", "AircraftJoystick.copyJoysticksIntoAircraft();");
	b = c.addChild("binding");
		b.set("command", "dialog-apply");
		b.set("object-name", "btnCopyAll");
	
	
	fgcommand("dialog-new", gui.dialog[_dlgName].prop());
	gui.showDialog(_dlgName);
};

var _onClose = func(){
	delete(gui.dialog,_dlgName);
	_saveDatabase();
};



var _database = nil;

var _loadDatabase = func(){
	var fileName = _writeFolder ~ "/AircraftJoystickDatabase.xml";
	var propName = "/sim/gui/dialogs/AircraftJoystick/database";
	_database = io.read_properties(fileName,propName);
	if (_database == nil){
		_database = props.globals.getNode(propName,1);
	}
};
var _saveDatabase = func(){
	var fileName = _writeFolder ~ "/AircraftJoystickDatabase.xml";
	var propName = "/sim/gui/dialogs/AircraftJoystick/database";
	
	io.write_properties(fileName,propName);
};


var _load = func(){
	#print("AircraftJoystick::_load() ... ");
	
	_loadDatabase();
	
	var osPath 	= os.path.new();
	var aircraft 	= "Aircraft_"~getprop("/sim/aircraft");
	var joysticks 	= _database.getNode(aircraft,1).getChildren("js");
	
	forindex (var i; joysticks) {
		var inFileName = joysticks[i].getNode("source",1).getValue();
		osPath.set(inFileName);
		#debug.dump("in",osPath.str);
		if(osPath.isFile()){
			var content = io.readfile(inFileName);
			var outFileName = sprintf("%s/Input/Joysticks/%03i_JAR.xml",getprop("/sim/fg-home"),i);
			
			osPath.set(outFileName);
			#debug.dump("out",osPath.str);
		
			if(osPath.exists(outFileName)){
				osPath.remove();
			}
			var file = io.open(outFileName, "w");
			io.write(file,content);
			io.close(file);
		}else{
			print("AircraftJoystick::_load() ... file("~osPath.str~") not found.");	
		}
	}
	
};

var _remove = func(){
	#print("AircraftJoystick::remove() ... ");
	
	var osPath = os.path.new();
	for (var i=0; i < 20 ; i+=1) {
		var outFileName = sprintf("%s/Input/Joysticks/%03i_JAR.xml",getprop("/sim/fg-home"),i);
		osPath.set(outFileName);
		#debug.dump("file",osPath.str);
		if(osPath.exists(outFileName)){
			osPath.remove();
		}
	}
};
var _copy = func(){
	#print("AircraftJoystick::copyAll() ... ");
	var inFile 	= os.path.new();
	var outFile 	= os.path.new();
	var joysticks 	= props.globals.getNode("/input/joysticks/").getChildren("js");
	var aircraft 	= "Aircraft_"~getprop("/sim/aircraft");
	
	_loadDatabase();
	_database.getNode(aircraft,1).removeAllChildren();
		
	forindex (var i; joysticks) {
		inFile.set(getprop("/input/joysticks/js["~i~"]/source"));
		if(inFile.isFile()){
			var content = io.readfile(inFile.str);
			
			outFile.set(_writeFolder);
			outFile.append(sprintf("/JPA_%s_js%s_%s",getprop("/sim/aircraft"),i,inFile.file));
			#debug.dump("out",outFile.str);
			if(outFile.canWrite()){
				var file = io.open(outFile.str, "w");
				io.write(file,content);
				io.close(file);
				_database.getNode(aircraft ~ "/js["~i~"]/source",1).setValue(outFile.str);
			}else{
				print( "Can't write to File "~ outFile.str);
			}
		}
	}
	
	_saveDatabase();
		
};


############## functions to call by gui ################

var toggleConfigDialog = func(){
	print("AircraftJoystick::toggleConfigDialog() ... ");
	_showConfigDialog("/sim/gui/dialogs/AircraftJoystick",1);
	
};
var loadJoystickByAircraftName = func(){
	print("AircraftJoystick::loadJoystickByAircraftName() ... ");
	_remove();
	_load();
	fgcommand("reinit", props.Node.new({"subsystem": "input"}));
};
var unloadAllJoysticks = func(){
	print("AircraftJoystick::unloadAllJoysticks() ... ");
	_remove();
	fgcommand("reinit", props.Node.new({"subsystem": "input"}));
};
var copyJoysticksIntoAircraft = func(){
	print("AircraftJoystick::copyJoysticksIntoAircraft() ... ");
	_copy();
};

############## Main ################
settimer(func {
	
	_loadDatabase();
	#AircraftJoystick._initMenu();
	_initMenu();

	if (getprop("/sim/gui/dialogs/AircraftJoystick/database/autoReload")){
		loadJoystickByAircraftName();
	}
},1.0);
